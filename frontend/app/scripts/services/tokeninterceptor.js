'use strict';

/* this is not used at all!!! modals are UIX-nightmare! */

/**
 * @ngdoc service
 * @name clndrApp.modalService
 * @description
 * # modalService
 * Service in the clndrApp.
 */
angular.module('frontendApp')

.factory('TokenInterceptor', ['$q', 'AuthFactory', function ($q, AuthFactory) {
	return {
		request: function (config) {
			config.headers = config.headers || {};
			var token = AuthFactory.getToken();
			var user = AuthFactory.getUser();
			// var uuid = AuthFactory.getUuid();

			if (token && user) {
				config.headers['X-Access-Token'] = token.token;
				config.headers['X-Key'] = user.email;
				config.headers['Content-Type'] = "application/json";
			}
			return config || $q.when(config);
		},

		response: function (response) {
			return response || $q.when(response);
		}
	};
}]);






