'use strict';

/* REST API service that fetches events from MYSQL database */

/**
 * @ngdoc service
 * @name clndrApp.clndrService
 * @description
 * # clndrService
 * Service in the clndrApp.
 */
angular.module('frontendApp')

.service('UserService', function ($rootScope, $q, $http, AuthFactory, API_URL) {		// , API_KEY

	this.getUuid = function () {
		return $http.get(API_URL + '/uuid', { cache: false });
	};

	this.login = function (user) {
		return $http.post(API_URL + '/login', user);
	};

	this.register = function(user) {
		return $http.post(API_URL + '/register', user);
	};

	this.logout = function() {
		AuthFactory.deleteAuth();
	};


	/*
  // we could do additional work here too
  return {
    sayHello: function () {
      console.log('hello');
    };
  }
	*/

});
