'use strict';

/* REST API service that fetches events from MYSQL database */

/**
 * @ngdoc service
 * @name clndrApp.clndrService
 * @description
 * # clndrService
 * Service in the clndrApp.
 */
angular.module('frontendApp')

.service('CartService', function ($rootScope, $q, $http, AuthFactory, API_URL) {		// API_KEY

	this.getCartItems = function() {
		var userId = null;
		if (AuthFactory.isLoggedIn()) {
			console.log('LoggedIn/getCartItems');
			userId = AuthFactory.getUser()._id;
			return $http.get(API_URL + '/users/' + userId + '/cart', { cache: false });
		} else {
			userId = $rootScope.uuid;
			return $http.get(API_URL + '/cart/' + userId, { cache: false });
		}
	};

	this.addToCart = function(product) {
		var userId = null;
		if (AuthFactory.isLoggedIn()) {
			console.log('LoggedIn/addToCart');
			userId = AuthFactory.getUser()._id;
			return $http.post(API_URL + '/users/' + userId + '/cart', product, { cache: false });
		} else {
			userId = $rootScope.uuid;
			return $http.post(API_URL + '/cart/' + userId, product, { cache: false });
		}
	};

	this.updateCartItemQuantity = function(cart_id, qty) {
		var userId = null;
		if (AuthFactory.isLoggedIn()) {
			console.log('LoggedIn/updateCartItemQuantity');
			userId = AuthFactory.getUser()._id;
			return $http.put(API_URL + "/users/" + userId + "/cart/" + cart_id + "/quantity/" + qty, { cache: false });
		} else {
			userId = $rootScope.uuid;
			return $http.put(API_URL + "/cart/" + userId + "/quantity/" + cart_id + "/qty/" + qty, { cache: false });
		}
	};

	this.removeFromCart = function(cart_id) {
		var userId = null;
		if (AuthFactory.isLoggedIn()) {
			console.log('LoggedIn/removeFromCart');
			userId = AuthFactory.getUser()._id;
			return $http.delete(API_URL + "/users/" + userId + "/cart/" + cart_id + "/remove", { cache: false });
		} else {
			userId = $rootScope.uuid;
			return $http.delete(API_URL + "/cart/" + userId + "/remove/" + cart_id, { cache: false });
		}
	};


	this.cartToUser = function (cart_id, user_id) {
		// /cart/:uuid/touser/:id
		return $http.get(API_URL + '/cart/' + cart_id + '/touser/' + user_id, { cache: false });
	};

	/*
  // we could do additional work here too
  return {
    sayHello: function () {
      console.log('hello');
    };
  }
	*/

});
