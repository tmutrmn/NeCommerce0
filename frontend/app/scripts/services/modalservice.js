'use strict';

/* REST API service that fetches events from MYSQL database */

/**
 * @ngdoc service
 * @name clndrApp.clndrService
 * @description
 * # clndrService
 * Service in the clndrApp.
 */
angular.module('frontendApp').service('ModalService', function ($uibModal, $log) { // .service('ModalService', function ($uibModal) {
	var modalInstance = null;

	var modalDefaults = {
		backdrop: true,
		keyboard: true,
		modalFade: true,
		templateUrl: 'modal.html' // templateUrl: '../views/modal.html'		// templateUrl: 'myModalContent.html'
	};

	var modalOptions = {
		size: 'sm',		// you can try different width like 'sm', 'md'
		closeButtonText: 'Close',
		actionButtonText: ' OK ',
		headerText: 'Proceed?',
		bodyText: 'Perform this action?'
	};


	this.open = function (opts) {
		modalInstance = $uibModal.open({
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			templateUrl: 'modal.html', // templateUrl: 'myModalContent.html',

			controller: function ($scope, $uibModalInstance) {
				$scope.modalOptions = opts;
				$scope.modalOptions.ok = function (result) {
					$uibModalInstance.close();
				},
				$scope.modalOptions.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};
			},

			size: opts.size || 'sm',
			keyboard: true,
			resolve: {
				someData: function () {
					return 'Return some Data';
				}
			}
		});

		modalInstance.result.then(function (result) {
			return result;
		}, function () {
			$log.info('Modal dismissed at: ' + new Date());
		});

		return modalInstance;
	};


	this.close = function () {
		modalInstance.close();
	};


});
