'use strict';

/* User Auth Factory */

angular.module('frontendApp')

.factory('AuthFactory', function ($localStorage) {		// API_URL, API_KEY
	//var userKey = 'user', tokenKey = 'token';
	var userKey = null,
			tokenKey = null,
			uuidKey = null;


	var AuthAPI = {
		isLoggedIn: function () {
			return this.getUser() == null ? false : true;
		},
		getUser: function () {
			userKey = $localStorage.userKey || null;
			return userKey;
		},
		setUser: function (user) {
			return $localStorage.userKey = user;
		},
		getToken: function () {
			tokenKey = $localStorage.tokenKey || null;
			return tokenKey;
		},
		setToken: function (token) {
			return $localStorage.tokenKey = token;
		},

		hasUuid: function () {
			// console.log('authfact hasUuid: ', this.getUuid());
			return this.getUuid() == null ? false : true;
		},
		getUuid: function () {
			uuidKey = $localStorage.uuidKey || null;
			return uuidKey;
		},
		setUuid: function (uuid) {
			// console.log('uuid:',uuid);
			return $localStorage.uuidKey = uuid;
		},

		deleteAuth: function () {
			delete $localStorage.userKey;
			delete $localStorage.tokenKey;
			delete $localStorage.uuidKey;
		}
	};
	return AuthAPI;
});
