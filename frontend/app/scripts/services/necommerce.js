'use strict';

/* REST API service that fetches events from MYSQL database */

/**
 * @ngdoc service
 * @name clndrApp.clndrService
 * @description
 * # clndrService
 * Service in the clndrApp.
 */
angular.module('frontendApp')
	/*.constant('API_URL', 'http://localhost:3000/api/v1')
	.constant('API_KEY', '1234567890') // faked at this point.*/


	.service('neCommerceService', function ($http, $q, API_URL) {		// , API_KEY

		/*** get all categories ***/
		this.getCategories = function () {
			var deferred = $q.defer();
			// console.log(API_URL + "/categories");
			$http.get(API_URL + "/categories", { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get a category ***/
		this.getCategory = function (id) {
			var deferred = $q.defer();
			$http.get(API_URL + "/categories/" + id, { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get all products in a category ***/
		this.getCategoryProducts = function (id) {
			var deferred = $q.defer();
			$http.get(API_URL + "/categories/" + id + "/products", { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get all products on sale ***/
		this.getProductsOnSale = function () {
			var deferred = $q.defer();
			$http.get(API_URL + "/products/onsale", { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get product by url_key... ***/
		this.getProduct = function (url_key) {
			var deferred = $q.defer();
			$http.get(API_URL + "/products/" + url_key, { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get product by id... ***/
		this.getProductById = function (id) {
			var deferred = $q.defer();
			$http.get(API_URL + "/products/" + id, { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get products in cart... ***/
		this.getProductsInCart = function (customer_id) {
			var deferred = $q.defer();
			$http.get(API_URL + "/products/" + customer_id, { cache: 'false' })
				.then(function (response) {
					deferred.resolve(response);
				}, function (error) {
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};





		/*** add to cart... ***/
		this.addToCart = function (customer_id) {
			var deferred = $q.defer();
			$http.post(API_URL + "/users/" + customer_id + "/cart", { cache: 'false' })
				.then(function (response) {
					console.log(response);
					deferred.resolve(response);
				}, function (error) {
					console.log(error);
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get cart... ***/
		this.getCart = function (customer_id) {
			var deferred = $q.defer();
			// /api/v1/users/:id/cart
			$http.get(API_URL + "/users/" + customer_id + "/cart", { cache: 'false' })
				.then(function (response) {
					console.log(response);
					deferred.resolve(response);
				}, function (error) {
					console.log(error);
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};


		/*** get cart... ***/
		this.removeFromCart = function (customer_id, cart_id) {
			var deferred = $q.defer();
			// /api/v1/users/:id/cart
			$http.delete(API_URL + "/users/" + customer_id + "/cart/" + cart_id + "/remove", { cache: 'false' })
				.then(function (response) {
					console.log(response);
					deferred.resolve(response);
				}, function (error) {
					console.log(error);
					deferred.reject("Error: " + error);
				});
			return deferred.promise;
		};

	});
