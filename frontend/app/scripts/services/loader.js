'use strict';

/* this is not used at all!!! modals are UIX-nightmare! */

/**
 * @ngdoc service
 * @name clndrApp.modalService
 * @description
 * # modalService
 * Service in the clndrApp.
 */

angular.module('frontendApp').factory('Loader', function ($timeout, ModalService) {
	var LOADER = {
		showLoading: function (text, bodyText) {
			text = text || 'Loading...';
			bodyText = bodyText || 'Please wait';

			var modalOptions = {
				closeButtonText: 'Cancel',
				actionButtonText: 'Ok',		// templateUrl: 'modal.html',
				headerText: text,
				bodyText: bodyText
			};
			ModalService.open(modalOptions);
		},

		hideLoading: function () {
			console.log('close modal');
			ModalService.close();
		},

		toggleLoadingWithMessage: function (text, bodyText, timeout) {
			var self = this;

			text = text || 'Loading...';
			bodyText = bodyText || 'Please wait';

			self.showLoading(text, bodyText);

			$timeout(function () {
				self.hideLoading();
			}, timeout || 3000);
		}
	};

	return LOADER;
});



