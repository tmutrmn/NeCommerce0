'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('NavigationCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout, neCommerceService, AuthFactory, UserService) {

	$scope.categories = [];
	$scope.productsOnSale = [];
	//$scope.cart = [];

	$scope.getCategories = function () {
		neCommerceService.getCategories()
			.then(function (response) {
				// console.log(response.data);
				$scope.categories = response.data;
			});
	};


	$scope.getCategories();

	if (AuthFactory.isLoggedIn()) {
		UserService.getCartItems().then(function (response) {		//.success(function (data) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage('Successfully added ' + $scope.book.title + ' to your cart', 2000);
			console.log(response);
			$rootScope.cart = response.data;
		}, function (error) {		//}).error(function (err, statusCode) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage(err.message);
			console.log(error.message);
		});
	}

});
