'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('CheckoutCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout,
																																		neCommerceService, AuthFactory, UserService, CartService) {		// ModalService) {

	$scope.products = [];
	$scope.image_url = null;
	$scope.categories = [];


	$scope.getCategories = function () {
		neCommerceService.getCategories()
			.then(function (response) {
				// console.log('getCategories', response.data);
				$scope.categories = response.data.data;
				console.log('categories', $scope.categories);
			});
	};

	$scope.getProductCategory = function (id) {
		var result = $rootScope.categories.filter(function(obj) {
  		return obj.id === id;
		})[0];
		//console.log(result);
		return result.url_key;
	};


	$scope.getProduct = function (category, url_key) {
		$scope.category = category;
		$scope.url_key = url_key;
		neCommerceService.getProduct(url_key)
			.then(function (response) {
				$scope.product = response.data[0];
				$scope.image_url = $scope.product.image;		//$scope.product.image.replace("/120/100", "/480/400")
				//console.log($scope.product);
			});
	};


	$scope.getCart = function (customer_id) {
		neCommerceService.getCart(customer_id)
			.then(function (response) {
				console.log(response);
				$rootScope.cart = response.data;
			});
	};


	$scope.removeFromCart = function (cart_id) {
		CartService.removeFromCart(cart_id).then(function (response) {
			console.log(response);
			// $scope.cart = response.data;
			var removeIndex = $rootScope.cart.map(function(item) { return item.cart_id; }).indexOf(cart_id);
			~removeIndex && $rootScope.cart.splice(removeIndex, 1);
		}, function (error) {		//}).error(function (err, statusCode) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage(err.message);
			console.log(error.message);
		});
	};



	$scope.qtyAmount = function(index, amount) {
		//console.log($scope.cart[index].qty, index, amount);
		$rootScope.cart[index].qty += amount;
		if ($rootScope.cart[index].qty <= 0) {
			$rootScope.cart[index].qty = 0;
			$scope.removeFromCart($rootScope.cart[index].cart_id);
		} else {
			console.log($rootScope.cart[index].cart_id, $rootScope.cart[index].qty);
			CartService.updateCartItemQuantity($rootScope.cart[index].cart_id, $rootScope.cart[index].qty).then(function (response) {
				//Loader.hideLoading();
				//Loader.toggleLoadingWithMessage('Successfully added ' + $scope.book.title + ' to your cart', 2000);
				console.log(response);
				// $rootScope.cart = response.data;
			}, function (error) {		//}).error(function (err, statusCode) {
				//Loader.hideLoading();
				//Loader.toggleLoadingWithMessage(err.message);
				console.log(error.message);
			});
		}
	};



	$scope.continueToCheckout = function() {

	};


	/*
	var user = AuthFactory.getUser();  // console.log(user);
	$scope.getCart(user._id);
	*/




	CartService.getCartItems().then(function (response) {
		//Loader.hideLoading();
		//Loader.toggleLoadingWithMessage('Successfully added ' + $scope.book.title + ' to your cart', 2000);
		console.log(response);
		$rootScope.cart = response.data;
		$scope.getCategories();
	}, function (error) {		//}).error(function (err, statusCode) {
		//Loader.hideLoading();
		//Loader.toggleLoadingWithMessage(err.message);
		console.log(error.message);
	});

});
