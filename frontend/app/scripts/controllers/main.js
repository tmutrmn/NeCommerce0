'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('MainCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout,
																																neCommerceService) {		//, AuthFactory, UserService, CartService, ModalService) {

	$scope.productsOnSale = [];
	$scope.categories = [];


	$scope.getCategories = function () {
		neCommerceService.getCategories()
			.then(function (response) {
				$scope.categories = response.data.data;
			});
	};


	$scope.getProductsOnSale = function () {
		neCommerceService.getProductsOnSale()
			.then(function (response) {
				$scope.productsOnSale = response.data.data;
			});
	};

	$scope.getProductCategory = function (id) {
		var result = $scope.categories.filter(function(obj) {
  		return obj.id === id;
		})[0];
		return result.url_key;
	};


	$scope.getCategories();
	$scope.getProductsOnSale();


	$scope.addToCart = function (product) {		// function (product)
		$rootScope.$broadcast('addToCart', { product: product });		// $scope.$broadcast('addToCart', { product: product });
	};

});
