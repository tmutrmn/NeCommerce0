'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('RegisterCtrl', function ($rootScope, $scope, AuthFactory, UserService) {

	$scope.user = {
		email: '',
		password: ''
	};

	$scope.register = function (callback) {
		UserService.register($scope.user).then(function (response) {
			var data = response.data;
			var status = response.status;
			var statusText = response.statusText;
			var headers = response.headers;
			var config = response.config;
			console.log(data,status,statusText,headers,config);

			console.log(data.user);
			AuthFactory.setUser(data.user);
			AuthFactory.setToken({
				token: data.token,
				expires: data.expires
			});

			$rootScope.isAuthenticated = true;
			$rootScope.message = "You have succesfully registered.";
			//Loader.hideLoading();
			//$scope.modal.hide();
			if (typeof callback === 'function') {
				callback();
			}
		}, function (error) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage(err.message);
			console.log(error.message);
		});

	};
});
