'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('CartCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout, Loader,
																																neCommerceService, AuthFactory, UserService, CartService, ModalService) {
	$scope.products = [];
	$scope.image_url = null;
	// $scope.cart = [];
	$scope.categories = [];
	// console.log($scope.categories.length)

	$scope.getCategories = function () {
		neCommerceService.getCategories()
			.then(function (response) {
				// console.log('getCategories', response.data);
				$scope.categories = response.data.data;
				console.log('categories', $scope.categories);
			});
	};

	$scope.getProductCategory = function (id) {
		var result = $scope.categories.filter(function(obj) {		// $rootScope.categories.filter(function(obj) {
  		return obj.id === id;
		})[0];
		return result.url_key;
	};

	$scope.getCart = function (customer_id) {
		console.log('cart.js $scope.getCart: ' + customer_id);
		CartService.getCartItems().then(function (response) {
			$rootScope.cart = response.data.data;
		}, function (error) {
			console.log(error.message);
		});
	};


	$scope.removeFromCart = function (cart_id) {
		CartService.removeFromCart(cart_id).then(function (response) {
			console.log('removeFromCart', response);
			var removeIndex = $rootScope.cart.map(function(item) { return item.cart_id; }).indexOf(cart_id);
				~removeIndex && $rootScope.cart.splice(removeIndex, 1);

			var modalOptions = {
				size: 'md',
				closeButtonText: null,
				actionButtonText: '  Ok  ',
				headerText: 'Item removed',
				bodyText: 'Item successfully removed from your cart',
			};
			ModalService.open(modalOptions);

		}, function (error) {		//}).error(function (err, statusCode) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage(err.message);
			console.log(error.message);
		});
	};



	$scope.qtyAmount = function(index, amount) {
		$rootScope.cart[index].qty += amount;
		if ($rootScope.cart[index].qty <= 0) {
			$rootScope.cart[index].qty = 0;
			$scope.removeFromCart($rootScope.cart[index].cart_id);
		} else {
			console.log($rootScope.cart[index].cart_id, $rootScope.cart[index].qty);
			CartService.updateCartItemQuantity($rootScope.cart[index].cart_id, $rootScope.cart[index].qty).then(function (response) {
				console.log('qtyAmount', response);
				// $rootScope.cart = response.data;

				var modalOptions = {
					size: 'md',
					closeButtonText: null,
					actionButtonText: '  Ok  ',
					headerText: 'Item quantity updated',
					bodyText: 'Item quantity successfully updated',
				};
				ModalService.open(modalOptions);

			}, function (error) {		//}).error(function (err, statusCode) {
				//Loader.hideLoading();
				//Loader.toggleLoadingWithMessage(err.message);
				console.log(error.message);
			});
		}
	};



	$scope.continueShopping = function() {
		$location.url('/');
	};


	$scope.continueToCheckout = function() {
		if (AuthFactory.isLoggedIn()) {
			$location.url('/checkout');
		} else {
			$location.url('/login-register?cntn=checkout');
		}
	};

	Loader.showLoading('Loading Cart...');
	CartService.getCartItems().then(function (response) {
		$timeout(function () {		// simulate wait
			Loader.hideLoading();
		}, 500);
		console.log('CartService.getCartItems', response);
		$rootScope.cart = response.data.data;
		$scope.getCategories();
	}, function (error) {		//}).error(function (err, statusCode) {
		Loader.hideLoading();
		Loader.toggleLoadingWithMessage(error.message);
		console.log(error.message);
	});

});
