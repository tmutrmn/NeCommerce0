'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp')
.controller('ProductCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout,
																			neCommerceService) {		//, AuthFactory, UserService, CartService, ModalService) {

	$scope.category = null;
	$scope.url_key = null;
	$scope.product = null;
	$scope.image_url = null;

	$scope.getProduct = function (category, url_key) {
		$scope.category = category;
		$scope.url_key = url_key;
		neCommerceService.getProduct(url_key)
			.then(function (response) {
				// console.log(response);
				$scope.product = response.data.data[0];
				$scope.image_url = $scope.product.image.replace("/120/100", "/480/400");
			});
	};


	$scope.addToCart = function (product) {		// function (product)
		$rootScope.$broadcast('addToCart', { product: product });
	};



	$scope.getProduct($routeParams.category, $routeParams.product);
});
