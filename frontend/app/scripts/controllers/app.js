'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('AppCtrl', function ($rootScope, $cookies, $scope, $window, $q, $location, $routeParams, $timeout,
																															neCommerceService, AuthFactory, UserService, CartService, Loader) {		// , ModalService
	$scope.selected = 'Home';

	$scope.select = function (item) {
		$scope.selected = item;
	};
	$scope.isActive = function (item) {
		// return $scope.selected === item;
		if ($scope.selected === item) { return true; }
		else { return false; }
	};


	$scope.categories = [];
	$scope.productsOnSale = [];
	//$scope.cart = [];

	$scope.getCategories = function () {
		neCommerceService.getCategories()
			.then(function (response) {
				$scope.categories = response.data.data;
			});
	};


	$scope.getCart = function () {
		CartService.getCartItems().then(function (response) {
			$rootScope.cart = response.data.data;
		}, function (error) {
			console.log(error.message);
		});
	};


	$scope.loginAndContinueToCheckout = function () {
		$scope.login( function() {
			console.log(AuthFactory.isLoggedIn(), $rootScope.uuid, AuthFactory.getUser()._id);

			CartService.cartToUser($rootScope.uuid, AuthFactory.getUser()._id).then(function (response) {
				console.log(response);
				$location.path('/checkout');
			}, function (error) {
				console.log(error, error.message);
			});
			//

		});
	};


	$scope.registerAndContinueToCheckout = function () {
		$scope.register(function() {
			$location.path('/checkout');
		});
	};


	$scope.login = function (callback) {
		Loader.showLoading('Authenticating...');

		UserService.login($scope.user).then(function (response) {
			var data = response.data.data;
			var status = response.status;
			var statusText = response.statusText;
			var headers = response.headers;
			var config = response.config;
			console.log(data,status,statusText,headers,config);

			console.log(data.user);
			AuthFactory.setUser(data.user);
			AuthFactory.setToken({
				token: data.token,
				expires: data.expires
			});

			if ($rootScope.uuid != null && AuthFactory.getUser()._id != null) {
				CartService.cartToUser($rootScope.uuid, AuthFactory.getUser()._id).then(function (response) {
					console.log('CartService.cartToUser', response);
					console.log($rootScope.cart);
				}, function (error) {		//}).error(function (err, statusCode) {
					console.log(error.message);
				});
			}

			$rootScope.isAuthenticated = true;
			$rootScope.error = null;
			$rootScope.message = response.data.message;
			Loader.hideLoading();

			if (typeof callback === 'function') {		// typeof callback === 'function' && callback();
				callback();
			}
		}, function (error) {
			Loader.hideLoading();
			Loader.toggleLoadingWithMessage(error.data.error, error.data.message);
			$rootScope.message = null;
			$rootScope.error = error.data.error + "\n" + error.data.message;
		});
	};



	$scope.register = function (callback) {
		console.log('1: ', $rootScope.uuid);
		$rootScope.uuid = $cookies.get("ne_uuid");
		$scope.user.uuid = $rootScope.uuid;
		console.log('2: ', $rootScope.uuid);
		console.log($scope.user);

		UserService.register($scope.user).then(function (response) {
			var data = response.data.data;
			var status = response.status;
			var statusText = response.statusText;
			var headers = response.headers;
			var config = response.config;
			console.log(data, status, statusText, headers, config);

			console.log(data.user);
			AuthFactory.setUser(data.user);
			AuthFactory.setToken({
				token: data.token,
				expires: data.expires
			});

			$rootScope.isAuthenticated = true;
			$rootScope.message = "You have succesfully registered.";
			//Loader.hideLoading();
			//$scope.modal.hide();
			if (typeof callback === 'function') {
				callback();
			}
		}, function (error) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage(err.message);
			console.log(error.message);
		});
	};


	$scope.logout = function() {
		UserService.logout();
		$rootScope.isAuthenticated = false;
		$location.path('/');
		Loader.toggleLoadingWithMessage('Successfully Logged Out!', null, 2000);
	};



	if (typeof $routeParams.cntn === "undefined") {
		console.log('routeParams == null');
	} else {
		console.log($routeParams.cntn);
	}


	$scope.getCategories();
});
