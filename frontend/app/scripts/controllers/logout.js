'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('LogoutCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout, neCommerceService, AuthFactory, UserService) {

	$scope.user = {
		email: '',
		password: ''
	};

	$rootScope.logout = function () {
		UserService.logout();
		$rootScope.isAuthenticated = false;
		$location.path('#/');
		$rootScope.message = 'Successfully Logged Out!';
	};

	$rootScope.logout();
});
