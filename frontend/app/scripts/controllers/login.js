'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('LoginCtrl', function ($rootScope, $scope, AuthFactory, UserService) {

	$scope.user = {
		email: '',
		password: ''
	};

	$scope.login = function (callback) {
		//Loader.toggleLoadingWithMessage('Authenticating...', 3000);
		// Loader.showLoading('Authenticating...');
		UserService.login($scope.user).then(function (response) {
			var data = response.data.data;
			var status = response.status;
			var statusText = response.statusText;
			var headers = response.headers;
			var config = response.config;
			console.log(data,status,statusText,headers,config);

			console.log(data);
			console.log(data.user);
			AuthFactory.setUser(data.user);
			AuthFactory.setToken({
				token: data.token,
				expires: data.expires
			});

			$rootScope.isAuthenticated = true;
			$rootScope.message = "You have succesfully logged in.";
			if (typeof callback === 'function') {
				callback();
			}
		}, function (error) {
			//Loader.hideLoading();
			//Loader.toggleLoadingWithMessage(err.message);
			console.log(error.message);
		});
	};

});
