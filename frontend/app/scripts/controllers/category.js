'use strict';

/**
 * @ngdoc function
 * @name frontendApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the frontendApp
 */
angular.module('frontendApp').controller('CategoryCtrl', function ($rootScope, $scope, $window, $q, $location, $routeParams, $timeout,
																																		neCommerceService) {		//, AuthFactory, UserService, CartService, ModalService) {

	$scope.categories = [];
	$scope.category = null;
	$scope.products = [];

	$scope.getCategory = function () {
		neCommerceService.getCategories()
			.then(function (response) {
				console.log($routeParams.category);
				$scope.categories = response.data.data;
				$scope.category = $routeParams.category;
				$scope.getProductsInCategory($scope.category);
			});
	};

	$scope.getProductsInCategory = function (cat) {
		cat = cat.toLowerCase();
		var result = $scope.categories.filter(function(obj) {
  		return obj.url_key === cat;
		})[0];
		neCommerceService.getCategoryProducts(result.id)
			.then(function (response) {
				// console.log('getProductsInCategory', response.data);
				$scope.products = response.data.data;
			});
	};


	$scope.addToCart = function (product) {		// function (product)
		$rootScope.$broadcast('addToCart', { product: product });		// $scope.$broadcast('addToCart', { product: product });
	};


	$scope.getCategory($routeParams.category);
});
