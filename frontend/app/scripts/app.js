'use strict';

/**
 * @ngdoc overview
 * @name frontendApp
 * @description
 * # frontendApp
 *
 * Main module of the application.
 */
angular
	.module('frontendApp', [
		'ngAnimate',
		'ngAria',
		'ngCookies',
		'ngMessages',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'ngTouch',
		'ui.bootstrap',
		'ngStorage',
	])

.constant('API_URL', 'http://localhost:3000/api/v1')
.constant('API_KEY', '1234567890') // faked at this point.

	.run(function($rootScope, $cookies, $window, $q, $location, $routeParams, $timeout,
								neCommerceService, AuthFactory, UserService, CartService, ModalService) {

		// $rootScope.categories = [];
		$rootScope.cart = [];
		$rootScope.message = null;
		$rootScope.error = null;

		$rootScope.isAuthenticated = AuthFactory.isLoggedIn();
		$rootScope.hasUuid = AuthFactory.hasUuid();

		$rootScope.uuid = $cookies.get('ne_uuid');
		if ($rootScope.uuid == null) {
			UserService.getUuid().then(function (response) {
				$rootScope.uuid = response.data;
				console.log('no uuid found, generating new one: ' + $rootScope.uuid);
				$cookies.put("ne_uuid", $rootScope.uuid);
			});
		} else {
			console.log('ne_uuid found: ' + $rootScope.uuid);
		}


		CartService.getCartItems().then(function (response) {
			$rootScope.cart = response.data.data;
			//$scope.getCategories();
		}, function (error) {		//}).error(function (err, statusCode) {
			console.log(error.message);
		});



		$rootScope.$on('addToCart', function (event, args) {		//, function(event, args) {		//var product = args.product;
			var product = args.product;

			var _product = $rootScope.cart.filter(function (obj) {
				return obj.id === product.id;
			})[0];

			if (_product == null) {
				CartService.addToCart({
					id: product.id,
					qty: 1
				}).then(function (response) {		//.success(function (data) {
					CartService.getCartItems().then(function (response) {		//.success(function (data) {
						console.log('addNewItem/addToCart', response.data);
						$rootScope.cart = response.data.data;

						var modalOptions = {
							size: 'md',
							closeButtonText: null,
							actionButtonText: '  Ok  ',
							// templateUrl: 'modal.html',
							headerText: 'Item added to your cart',
							bodyText: 'Successfully added ' + product.product_name + ' to your cart',
						};
						ModalService.open(modalOptions);

					}, function (error) {		//}).error(function (err, statusCode) {
						console.log(error.message);
					});
				}, function (error) {		//}).error(function (err, statusCode) {
					console.log(error.message);
				});
			} else {
				var _index = $rootScope.cart.findIndex(function (obj) {
					return obj === _product;
				}, _product);
				$rootScope.cart[_index].qty++;
				CartService.updateCartItemQuantity($rootScope.cart[_index].cart_id, $rootScope.cart[_index].qty).then(function (response) {
					CartService.getCartItems().then(function (response) {		//.success(function (data) {
						console.log('addExistingItem/addToCart', response);
						$rootScope.cart = response.data.data;

						var modalOptions = {
							closeButtonText: 'Cancel',
							actionButtonText: 'Ok',		// templateUrl: 'modal.html',
							headerText: 'Item added to your cart',
							bodyText: 'Successfully added ' + product.product_name + ' to your cart'
						};
						ModalService.open(modalOptions);

					}, function (error) {		//}).error(function (err, statusCode) {
						console.log(error.message);
					});
				}, function (error) {		//}).error(function (err, statusCode) {
					console.log(error.message);
				});
			}
		});



		$rootScope.logout = function () {
			UserService.logout();
			$rootScope.isAuthenticated = false;
			$location.path('#/');
			$rootScope.message = 'Successfully Logged Out!';
		};

		$rootScope.getNumber = function(num) {	// utility method to convert number to an array of elements
			return new Array(num);
		};

	})		// run


	.config(function ($routeProvider, $locationProvider, $httpProvider) {
		$httpProvider.interceptors.push('TokenInterceptor');
		$routeProvider
			.when('/', {
				templateUrl: 'views/main.html',
				controller: 'MainCtrl',
				controllerAs: 'main'
			})
			.when('/categories', {
				cache: false,
				templateUrl: 'views/categories.html',
				controller: 'CategoriesCtrl',
				controllerAs: 'categories'
			})
			.when('/category/:category', {
				cache: false,
				templateUrl: 'views/category.html',
				controller: 'CategoryCtrl',
				controllerAs: 'category'
			})
			.when('/:category/:product', {
				cache: false,
				templateUrl: 'views/product.html',
				controller: 'ProductCtrl',
				controllerAs: 'product'
			})

			.when('/sale', {
				templateUrl: 'views/sale.html',
				controller: 'SaleCtrl',
				controllerAs: 'sale'
			})
			.when('/about', {
				templateUrl: 'views/about.html',
				controller: 'AboutCtrl',
				controllerAs: 'about'
			})

			.when('/cart', {
				templateUrl: 'views/cart.html',
				controller: 'CartCtrl',
			})
			.when('/checkout', {
				templateUrl: 'views/checkout.html',
				controller: 'CheckoutCtrl',
			})

			.when('/login', {
				templateUrl: 'views/login.html',
				controller: 'AppCtrl'
			})
			.when('/register', {
				templateUrl: 'views/register.html',
				controller: 'AppCtrl'
			})
			.when('/login-register', {
				templateUrl: 'views/login-register.html',
				controller: 'AppCtrl'
			})
			.when('/logout', {
				templateUrl: 'views/logout.html',
				controller: 'AppCtrl'
			})

			.otherwise({
				redirectTo: '/'
			});

			$locationProvider.hashPrefix('');
	});
