var fs = require('fs');
var json2csv = require('json2csv');
var faker = require('faker');
var shortid = require('shortid');

var data = [];

// generate 30 random product
for (var j = 1; j <= 10; j++) {
for (var i = 0; i < 30; i++) {
var tmpProd = faker.commerce.productName();
var tmpId = (j-1)*30 + i;
var product = {
id: tmpId,
uuid: shortid.generate(),
product_name:	tmpProd,
description: faker.lorem.paragraph(2),
color: faker.commerce.color(),
image: faker.image.image(120,100,true),
url_key: tmpProd.toLowerCase(),
sku: tmpProd.substring(0, 8) + tmpId,
price: faker.commerce.price(),
quantity: Math.floor(Math.random() * (100 - 1 + 1)) + 1,
category_id: j,
on_sale: Math.floor(Math.random() * (100 - 1 + 1)) + 1 > 80 ? true : false,
promotion_code: faker.address.countryCode() + faker.address.zipCode()
}
data.push(product);
}
};

 
var fields = ['id', 'uuid', 'product_name', 'description', 'color', 'image', 'url_key', 'sku', 'price', 'quantity', 'category_id', 'on_sale', 'promotion_code'];
 
try {
	var opts = {
		data: data,
		fields: fields,
		quotes: ''
	};
  var csv = json2csv(opts);
  // console.log(result);
	fs.writeFile('file2.csv', csv, function(err) {
		if (err) throw err;
		console.log('file saved');
	});
} catch (err) {
  // Errors are thrown for bad options, or if the data is empty and no fields are provided. 
  // Be sure to provide fields if it is possible that your data array will be empty. 
  console.error(err);
}
