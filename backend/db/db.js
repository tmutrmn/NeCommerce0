var mysql = require('mysql2');

// var pool = mysql.createPool(config.db);
var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: 'localhost',
    user: 'necommerce_u',
    password: 'necommerce_p',
    database: 'necommerce',
    debug: false
});

var connection = {

	query: function (query, params, callback) {
		pool.getConnection(function (err, connection) {
			console.log("connected as id: " + connection.threadId);
			if (err) {
				connection.release();
				callback(null, err);
				throw err;
			}

			connection.query(query, params, function (err, rows) {
				connection.release();
				if (!err) {
					callback(rows, err);		// callback(rows);
				}
				else {
					callback(null, err);
				}

			});

			connection.on('error', function (err) {
				connection.release();
				callback(null, err);
				throw err;
			});
		});
	}


	/*
	query: function () {
		var queryArgs = Array.prototype.slice.call(arguments),
			events = [],
			eventNameIndex = {};

		pool.getConnection(function (err, conn) {
			if (err) {
				if (eventNameIndex.error) {
					eventNameIndex.error();
				}
			}
			if (conn) {
				var q = conn.query.apply(conn, queryArgs);
				q.on('end', function () {
					conn.release();
				});

				events.forEach(function (args) {
					q.on.apply(q, args);
				});
			}
		});

		return {
			on: function (eventName, callback) {
				events.push(Array.prototype.slice.call(arguments));
				eventNameIndex[eventName] = callback;
				return this;
			}
		};
	}
	*/

};

module.exports = connection;