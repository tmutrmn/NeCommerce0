var fs = require('fs');
var json2csv = require('json2csv');
var faker = require('faker');

var data = [];

for (var i = 0; i < 32; i++) {
	var tmpCat = faker.commerce.department(1, false);
	var category = {
		id: i+1,
		category: tmpCat,
		url_key: tmpCat.toLowerCase(),
		description: faker.lorem.paragraph(2),
		image: ''
	}
	data.push(category);
	console.log(category);
}

var fields = ['id', 'category', 'url_key', 'description', 'image'];
 
try {
	var opts = {
		data: data,
		fields: fields,
		quotes: ''
	};
  var csv = json2csv(opts);
  // console.log(result);
	fs.writeFile('file.csv', csv, function(err) {
		if (err) throw err;
		console.log('file saved');
	});
} catch (err) {
  // Errors are thrown for bad options, or if the data is empty and no fields are provided. 
  // Be sure to provide fields if it is possible that your data array will be empty. 
  console.error(err);
}
