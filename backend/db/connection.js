var mysql = require('mysql2');

var pool = mysql.createPool({
    connectionLimit: 100, //important
    host: 'localhost',
    user: 'necommerce_u',
    password: 'necommerce_p',
    database: 'necommerce',
    debug: false
});

module.exports = pool;