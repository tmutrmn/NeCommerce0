var jwt = require('jwt-simple');
var validate = require('../routes/auth.js').validate;
var respHandler = require('../utils/responseHandler.js');

module.exports = function(req, res, next) {

    var token = (req.body && req.body.access_token) || (req.query && req.query.access_token) || req.headers['x-access-token'];
    var key = (req.body && req.body.x_key) || (req.query && req.query.x_key) || req.headers['x-key'];

    if (token && key) {

        try {
            var decoded = jwt.decode(token, require('../utils/secret.js'));

            if (decoded.exp <= Date.now()) {
                respHandler(res, 400, null, 'Token Expired', 'Token Expired');
                return;
            }
            var reqUser = decoded.user;

            if (reqUser.email !== key) {
                respHandler(res, 401, null, 'Invalid Token or Key', 'Invalid Token or Key');
                return;
            } else {
                validate(reqUser._id, function(err, user) {
                    if (err) {
                        respHandler(res, 500, null, 'Oops, something went wrong!', 'Oops, something went wrong!');
                        return;
                    } else {
                        if (!user) {
                            respHandler(res, 400, null, 'Invalid User', 'Invalid User');
                            return;
                        } else {
                            next();
                        }
                    }
                });
            }
        } catch (err) {
            respHandler(res, 500, null, 'Oops, something went wrong!', 'Oops, something went wrong!');
            return;
        }
    } else {
        respHandler(res, 401, null, 'Invalid Token or Key', 'Invalid Token or Key');
        return;
    }
}
