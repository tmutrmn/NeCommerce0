// var compression = require('compression');
var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var RateLimit = require('express-rate-limit');
var cors = require('cors');
var uid = require('uid-safe');

var app = express();
// app.use(compression());		// use gzip compression


app.use(logger('dev'));
app.use(bodyParser.urlencoded({
	extended: false
}));
app.use(bodyParser.json());
app.use(cors());


var limiter = RateLimit({
	// window, delay, and max apply per-ip unless global is set to true 
	windowMs: 60 * 1000, // miliseconds - how long to keep records of requests in memory 
	delayMs: 1000, // milliseconds - base delay applied to the response - multiplied by number of recent hits from user's IP 
	max: 100, // max number of recent connections during `window` miliseconds before (temporarily) bocking the user. 
	global: false // if true, IP address is ignored and setting is applied equally to all requests 
});


app.use(express.static(__dirname + '/docs'));
app.use(express.static(__dirname + '/public'));


app.all('*', function (req, res, next) {
	// CORS headers
	res.header('Access-Control-Allow-Origin', '*'); // restrict it to the required domain
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	// Set custom headers for CORS
	res.header('Access-Control-Allow-Headers', 'Content-type, Accept, X-Access-Token, X-Key');
	// When performing a cross domain request, you will recieve a preflighted request first. 
	// This is to check if our the app is safe. 
	if (req.method == 'OPTIONS') {
		res.status(200).end();
	} else {
		next();
	}
});


app.use('/', require('./routes'));		// load app routes 

app.use(function (req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});



app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function () {
	console.log('Express server listening on port ' + server.address().port);
});