var jwt = require('jwt-simple');
// var db = require('../db/connection.js');
var db = require('../db/db.js');		// must do better!
var pwdMgr = require('../utils/managePassword.js');
var respHandler = require('../utils/responseHandler.js');

var auth = {

	register: function (req, res) {
		var user = req.body;
		var email = user.email || '';
		var password = user.password || '';

		// name is optional
		if (email.trim() == '' || password.trim() == '') {
			respHandler(res, 401, null, 'Invalid Credentials', 'Invalid Credentials');		// res, status, data, message, err
			return;
		}

		pwdMgr.cryptPassword(user.password, function (err, hash) {
			user.password = hash;

			var _customer = {
				email: email,
				password: user.password,
				uuid: user.uuid,
				first_name: user.first_name || '',
				last_name: user.last_name || '',
				company: user.company || '',
				phone_number: user.phone_number || '',
				country: user.country || '',
				state: user.state || '',
				city: user.city || '',
				postcode: user.postcode || '',
				street: user.street || ''
			};
			//console.log(_customer);
			
			db.query('INSERT INTO `customer` SET ?', _customer, function (data, error) {
				if (data == null) {
					respHandler(res, 403, null, 'Invalid User credentials', 'Email address already in use');
					return;
				} else if (data != null) {
					db.query("SELECT * FROM `customer` WHERE `id`=? LIMIT 1", data.insertId, function (data, error) {
						if (data == null || data.length == 0) {
							respHandler(res, 403, null, 'Invalid User credentials', 'Email address already in use');
							return;
						} else if (data != null && data.length > 0) {
							delete data[0].password;
							var _user = {
								_id: data[0].id,
								email: email
							}
							respHandler(res, 200, genToken(_user), 'Success', null);
						} else if (error) {
							respHandler(res, 100, null, 'Error in connection database', error);
							return;
						}
					});
				} else if (error) {
					respHandler(res, 100, null, 'Error in connection database', error);
					return;
				}
			});

		});

	},



	login: function (req, res) {
		var user = req.body;
		var email = user.email || '';
		var password = user.password || '';

		if (email.trim() == '' || password.trim() == '') {
			respHandler(res, 401, null, 'Invalid Credentials', 'Invalid Credentials');		// res, status, data, message, err
			return;
		}

		db.query("SELECT `id`, `email`, `password` FROM `customer` WHERE `email`=? LIMIT 1", email, function (data, error) {
			if (data == null || data.length == 0) {
				console.log('wrong email: ' + data);
				respHandler(res, 403, null, 'Wrong email address', 'Invalid User credentials');		// res, status, data, message, err
				return;
			} else if (data != null && data.length>0) {
				
				console.log(data);
				pwdMgr.comparePassword(password, data[0].password, function (err, isPasswordMatch) {
					console.log(err, isPasswordMatch);
					if (err) {
						respHandler(res, 403, null, 'Invalid User credentials', 'Invalid User credentials');
						return;
					} else if (isPasswordMatch) {
						delete data[0].password;
						console.log('passwd matched: ', data[0]);
						var _user = {
							_id: data[0].id,
							email: email
						}
						respHandler(res, 200, genToken(_user), 'Successfully logged in', null);
						return;
					} else if (!isPasswordMatch) {
						respHandler(res, 403, null, 'Wrong password', 'Invalid User credentials');
						return;
					}
				});

			} else if (error){
				respHandler(res, 100, null, 'Error in connection database', error);
				return;
			}
		});
	},

	validate: function (id, callback) {
		db.users.findOne({
			_id: require('mongojs').ObjectId(id)
		}, callback);
	}

}

// private method
function genToken(user) {
	console.log(user);
	var expires = expiresIn(7); // 7 days
	var token = jwt.encode({
		exp: expires,
		user: user,
	}, require('../utils/secret'));
	return {
		token: token,
		expires: expires,
		user: user
	};
}

function expiresIn(numDays) {
	var dateObj = new Date();
	return dateObj.setDate(dateObj.getDate() + numDays);
}

module.exports = auth;