// var db = require('../db/connection.js');
var db = require('../db/db.js');
var respHandler = require('../utils/responseHandler.js');

var Cart = {

	viewCart: function (req, res) {
		var customer_uuid = req.params.uuid;
		if (!customer_uuid) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}

		db.query("SELECT A.*, B.qty, B.id AS cart_id FROM products A JOIN cart B ON A.id = B.product_id WHERE B.customer_uuid=?", customer_uuid, function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},


	removeFromCart: function (req, res) {
		var customer_uuid = req.params.uuid;
		var cart_id = req.params.cart_id;
		if (!customer_uuid || !cart_id) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');
			return;
		}
		
		db.query('DELETE FROM `cart`  WHERE `id`=? AND `customer_uuid`=? LIMIT 1', [cart_id, customer_uuid], function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {		
				// console.log('deleted ' + rows.affectedRows + ' rows');
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},


	addToCart: function (req, res) {
		var customer_uuid = req.params.uuid;
		var productDetails = req.body;

		if (!customer_uuid || !productDetails) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}

		cart_item = {
			customer_id: 0,
			customer_uuid: customer_uuid,
			product_id: productDetails.id,
			qty: productDetails.qty
		}

		db.query('INSERT INTO `cart` SET ?', cart_item, function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},


	transferCartToRegisteredUser: function (req, res) {
		var customer_uuid = req.params.uuid;
		var user_id = req.params.id;
		console.log(customer_uuid,user_id);
		if (!customer_uuid || !user_id) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}

		db.query("UPDATE `cart` SET `customer_id`=?, `status`=`transferredCartToRegisteredUser` WHERE `customer_uuid`=?", [user_id, customer_uuid], function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
		
	},


	updateQuantity: function (req, res) {
		var customer_uuid = req.params.uuid;
		var cart_id = req.params.cart_id;
		var qty = req.params.qty;

		if (!customer_uuid || !cart_id || !qty) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}

		db.query('UPDATE `cart` SET `qty`=? WHERE `id`=? AND `customer_uuid`=? LIMIT 1', [qty, cart_id, customer_uuid], function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	}
	
}

module.exports = Cart;