// var db = require('../db/connection.js');
var db = require('../db/db.js');
var respHandler = require('../utils/responseHandler.js');


var Products = {

	getProductsOnSale: function (req, res) {
		db.query("SELECT * FROM `products` WHERE `on_sale`=1 ORDER BY `category_id`", null, function (data, error) {
			if (error) {
				respHandler(res, 100, null, 'Error in connecting database', error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},

	getProduct: function (req, res) {
		var url_key = req.params.url_key; 
		db.query("SELECT * FROM `products` WHERE `url_key`=? LIMIT 1", url_key, function (data, error) {
			if (error) {
				respHandler(res, 100, null, 'Error in connecting database', error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}
		});
	}

}

module.exports = Products;