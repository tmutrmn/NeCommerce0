var express = require('express');
var router = express.Router();
var path = require('path');
var uid = require('uid-safe');
var cookieSession = require('cookie-session');

var auth = require('./auth.js');

var categories = require('./categories.js');
var products = require('./products.js');
var cart = require('./cart.js');
var userCart = require('./user-cart.js');

/*
 * Routes that can be accessed by any one
 */

router.use(cookieSession({
  name: 'NeCommerce-session',
  keys: ['key1', 'key2'],
  // Cookie Options
  maxAge: 24 * 60 * 60 * 1000,	 // 24 hours
	sameSite: 'lax'
}))


router.get('/api/v1/cookie', function (req, res, next) {
	req.session.views = (req.session.views || 0) + 1
	console.log(req.session.views);
	res.end(req.session.views + ' views')	// Write response
});

router.get('/api/v1/uuid', function (req, res, next) {
	req.session.ne_uuid = req.session.ne_uuid || "";
	if (req.session.ne_uuid == "") {		// if (typeof variable === 'undefined' || variable === null) {
		var string = uid.sync(18)
		req.session.ne_uuid = string;
	}
	res.end(req.session.ne_uuid);
});

router.get('/api/v1/categories', categories.getAll);
router.get('/api/v1/categories/:id', categories.getCategory);
// router.get('/api/v1/categories/:catname', categories.getByCatName);
router.get('/api/v1/categories/:id/products', categories.getProducts);

router.get('/api/v1/products/onsale', products.getProductsOnSale);
router.get('/api/v1/products/:url_key', products.getProduct);


router.post('/api/v1/login', auth.login);
router.post('/api/v1/register', auth.register);


// shopping cart routes for authenticated user
router.get('/api/v1/users/:user_id/cart', userCart.viewCart);
router.post('/api/v1/users/:user_id/cart', userCart.addToCart);
router.delete('/api/v1/users/:user_id/cart/:cart_id/remove', userCart.removeFromCart);
router.put('/api/v1/users/:user_id/cart/:cart_id/quantity/:qty', userCart.updateQuantity);


// shopping cart routes for non-authenticated user
router.get('/api/v1/cart/:uuid', cart.viewCart);
router.post('/api/v1/cart/:uuid', cart.addToCart);
router.delete('/api/v1/cart/:uuid/remove/:cart_id', cart.removeFromCart);
router.put('/api/v1/cart/:uuid/quantity/:cart_id/qty/:qty', cart.updateQuantity);
router.get('/api/v1/cart/:uuid/touser/:id', cart.transferCartToRegisteredUser);		// should use post or put with some confirmation post value


router.get('/*', function (req, res, next) {
	res.json({
		"code": 200,
		"status": "nothing to see here"
	});
});

module.exports = router;