// var db = require('../db/connection.js');
var db = require('../db/db.js');
var respHandler = require('../utils/responseHandler.js');


var Categories = {

	getAll: function (req, res) {
		db.query("SELECT `id`, `category`, `url_key`, `description`, `image` FROM `categories`", null, function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},

	getCategory: function (req, res) {
		var cat_id = req.params.id;
		if (!isNaN(cat_id)) {
			cat_id = parseInt(req.params.id);
			db.query("SELECT `id`, `category`, `url_key`, `description`, `image` FROM `categories` WHERE `id`=? LIMIT 1", cat_id, function (data, error) {
				if (error) {
					respHandler(res, 100, null, "Error in connecting database", error);
					return;
				} else {
					respHandler(res, 200, data, 'Success', null);
					return;
				}		// callback(data, error);
			});
		} else if (typeof cat_id === 'string' || cat_id instanceof String) {
			db.query("SELECT `id`, `category`, `url_key`, `description`, `image` FROM `categories` WHERE `category`=? LIMIT 1", cat_id, function (data, error) {
				if (error) {
					respHandler(res, 100, null, "Error in connecting database", error);
					return;
				} else {
					respHandler(res, 200, data, 'Success', null);
					return;
				}		// callback(data, error);
			});
		}
	},


	getProducts: function (req, res) {
		var cat_id = parseInt(req.params.id);
		db.query("SELECT * FROM `products` WHERE `category_id`=?", cat_id, function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error in connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	}

}

module.exports = Categories;