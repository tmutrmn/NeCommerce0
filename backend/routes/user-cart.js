// var db = require('../db/connection.js');
var db = require('../db/db.js');
var respHandler = require('../utils/responseHandler.js');

var Cart = {

	viewCart: function (req, res) {
		var customer_id = req.params.user_id;
		if (!customer_id) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}

		db.query("SELECT A.*, B.qty, B.id AS cart_id FROM products A JOIN cart B ON A.id = B.product_id WHERE B.customer_id=?", customer_id, function (data, error) {
			if (error) {
				respHandler(res, 100, null, "Error connecting database", error);
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},


	removeFromCart: function (req, res) {
		var customer_id = req.params.user_id;
		var cart_id = req.params.cart_id;
		if (!customer_id || !cart_id) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}
		
		db.query('DELETE FROM `cart`  WHERE `id`=? AND `customer_id`=? LIMIT 1', [cart_id, customer_id], function (data, error) {
			if (error) {
				respHandler(res, 100, null, 'Error in connecting database', error);		// res.json({ "code": 100, "status": "Error in connecting to database" });
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);		//		res.json(data); 	// respHandler(res, 200, { categories: rows }, 'Success', null);
				return;
			}		// callback(data, error);
		});
	},


	addToCart: function (req, res) {
		var customer_id = req.params.user_id;
		// var customer_id = customer_uuid;
		var productDetails = req.body; // book id, qty

		if (!customer_id || !productDetails) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');		// res, status, data, message, err
			return;
		}

		cart_item = {
			customer_id: customer_id,
			customer_uuid: 0,		// customer_uuid,
			product_id: productDetails.id,
			qty: productDetails.qty
		}

		db.query('INSERT INTO `cart` SET ?', cart_item, function (data, error) {
			if (error) {
				respHandler(res, 100, null, 'Error in connecting database', error);		// res.json({ "code": 100, "status": "Error in connecting to database" });
				return;
			} else {		
				// console.log('deleted ' + rows.affectedRows + ' rows');
				respHandler(res, 200, data, 'Success', null);			// res.json(data);
				return;
			}		// callback(data, error);
		});
	},


	updateQuantity: function (req, res) {
		var customer_id = req.params.user_id;
		var cart_id = req.params.cart_id;
		var qty = req.params.qty;

		console.log(customer_id,cart_id,qty);
		if (!customer_id || !cart_id || !qty) {
			respHandler(res, 400, null, 'Invalid input', 'Invalid input');
			return;
		}

		db.query('UPDATE `cart` SET `qty`=? WHERE `id`=? AND `customer_id`=? LIMIT 1', [qty, cart_id, customer_id], function (data, error) {
			if (error) {
				respHandler(res, 100, null, 'Error in connecting database', error);		// res.json({ "code": 100, "status": "Error in connecting to database" });
				return;
			} else {
				respHandler(res, 200, data, 'Success', null);
				return;
			}		// callback(data, error);
		});
	}
	
}

module.exports = Cart;