"use strict";
import React from 'react';
import { Router, IndexRoute, Route, Link, browserHistory } from "react-router";

import Layout from "./layout.js";

import Home from "./pages/home.js";
import Category from "./pages/category.js";

import Hello from './hello.js';
// import About from "./about.js";




const Greeting = () => <div>Hi there!</div>

/*
const Element = (
  <div>
    <h1>Hello, world!</h1>
    <h2>It is {new Date().toLocaleTimeString()}.</h2>
  </div>
);
*/

const NotFound = () => (
	<h1>404.. This page is not found!</h1>
)

const Address = () => (
	<h2>Address</h2>
)


const App = () => ( <div><Home /><Greeting /></div> )

const About = () => (
	<div>
		<h1>About</h1>
		<p>xzcvgxc g dsfdsfdas  fsda fsd fsdaf asd asfde asfd</p>
		<Link to='/'>&laquo; back</Link>
	</div>
)


const Routes = (
  <Router history={browserHistory}>
		<Route component={Layout}>
      <Route name="home" path="/" component={Home} />
      <Route name="category" path='/category/:cat' component={Category} />
      <Route name="hello" path="/hello" component={Hello} />
      <Route name="about" path="/about" component={About} />
      <Route path="*" component={NotFound} />
    </Route>
	</Router>
);

// <IndexRoute component={Home} />
//<Route path="*" component={NotFound} />
export default Routes;