"use strict";
import React from "react";
import Router from "react-router";
import { render } from "react-dom";

import Routes from "./routes.js";


render(
	Routes,
  document.getElementById("react")
)