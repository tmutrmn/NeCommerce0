"use strict";

import React from "react";


class Footer extends React.Component {
	render() {
		return (
			<footer className="footer text-center">
				<div className="container">
					<p className="text-muted">
						Copyright &copy; 2017. All lefts reserved.
					</p>
				</div>
			</footer>
		);
	}
};

module.exports = Footer;