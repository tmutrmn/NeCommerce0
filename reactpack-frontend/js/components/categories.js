"use strict";

import React from "react";

import CategoryItem from "./category-item";


class Categories extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			// id: this.props.routeParams.id,
			categories:	this.props.categories
		};
		//console.log('Categories component: ', this.props.categories);
	}

/*
	componentWillMount() {
		// do setStates and load ajax shit here
	}

	componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}
*/

	render() {
		return (
			<div>
				<h2>categories</h2>
				<ul>
					{
						this.state.categories.map(item => <CategoryItem key={item.id} id={item.id} title={item.category} url_key={item.url_key}  />)
					}
				</ul>

			</div>
		);
	}
}

module.exports = Categories;