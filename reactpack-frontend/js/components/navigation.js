"use strict";

import React from "react";
import { Nav, NavDropdown, NavItem, Navbar, MenuItem, Button } from 'react-bootstrap';;
import { NavLink , Link } from "react-router";
import { IndexLinkContainer, LinkContainer } from "react-router-bootstrap";



class Navigation extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			// comments:[] 
		};
  }

	render() {
		return (
			<header className="header">
				<Navbar collapseOnSelect>
					<Navbar.Header>
						<Navbar.Brand>
							<a className="navbar-brand" href="/">
								<img alt="Brand" src="/images/logo.png" />
							</a>
						</Navbar.Brand>
						<Navbar.Toggle />
					</Navbar.Header>
						
					<Navbar.Collapse>
						<div className="cartContainer">
						<Nav pullRight>
							<NavDropdown eventKey={2} title="Cart" id="basic-nav-dropdown">
								<MenuItem eventKey={3.1}>Item 1</MenuItem>
								<MenuItem eventKey={3.2}>Item 2</MenuItem>
								<MenuItem eventKey={3.3}>Item 3</MenuItem>
								<MenuItem divider />
								<MenuItem eventKey={3.3}>View Cart</MenuItem>
							</NavDropdown>
						</Nav>
						</div>

						<Nav pullLeft>
							<NavItem eventKey={1} href="/">Home</NavItem>
							<NavDropdown eventKey={2} title="Categories" id="basic-nav-dropdown">
								<MenuItem eventKey={3.1}>Action</MenuItem>
								<MenuItem eventKey={3.2}>Another action</MenuItem>
								<MenuItem eventKey={3.3}>Something else here</MenuItem>
								<MenuItem divider />
								<MenuItem eventKey={3.3}>Separated link</MenuItem>
							</NavDropdown>
							<NavItem eventKey={4} href="/hello" target="_self">Sale</NavItem>
							<NavItem eventKey={5} href="/about" target="_self">About</NavItem>
						</Nav>
						

						<Nav pullRight>
							<NavItem eventKey={1} href="#">Link Right</NavItem>
							<NavItem eventKey={2} href="#">Link Right</NavItem>
						</Nav>
					</Navbar.Collapse>
				</Navbar>
			</header>
		);
	}
}

module.exports = Navigation;