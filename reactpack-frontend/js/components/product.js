"use strict";

import React from "react";


class Product extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			cat: null,
			cat_id: this.props.category_id,
			categories:	this.props.categories 
		};
		// console.log(this.props.category_id)
	}

	componentWillMount() {
		this.state.cat = this.state.categories.find(obj => obj.id === this.state.cat_id);
	}

	componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}

	render() {
		return (
			<div>
				<h5>{ this.state.cat.category }</h5>
				<h3>
					<a href="#">{this.props.product_name}</a>
				</h3>
				<img src={this.props.image} />
				<p>{this.props.description}</p>
				<a className="btn btn-lg btn-primary" href={this.state.cat.category.toLowerCase()+"/"+this.props.url_key}>&nbsp;View&nbsp;</a>
				&nbsp;&nbsp;
				<a className="btn btn-lg btn-success">Buy &nbsp;<span className="glyphicon glyphicon-ok"></span></a>
			</div>
		);
	}
}

module.exports = Product;