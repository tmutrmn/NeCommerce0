"use strict";

import React from "react";
import { Router, Route, Link } from 'react-router'


class CategoryItem extends React.Component {
  render() {
    return (
      <li>
        <a href={`/category/${this.props.url_key}`}>{this.props.id}, {this.props.title}</a>
      </li>
    );
  }
};
//<Link to={`/category/${this.props.url_key}`}>{this.props.title}</Link>
export default CategoryItem;