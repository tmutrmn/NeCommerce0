"use strict";

import React from "react";
import { Grid, Row, Col, Jumbotron } from "react-bootstrap";

import Categories from "../components/categories";
import Product from "../components/Product";


class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			categories: [],
			products: []
		};
	}

	componentWillMount() {
		/* should use FLUX, REDUX or GraphSQL... */
		$.ajax({
			url: "http://localhost:3000/api/v1/categories",
			cache: false
		}).then(function (response) {
			this.setState({
				categories: response.data
			});
		}.bind(this));

		$.ajax({
			url: "http://localhost:3000/api/v1/products/onsale",
			cache: false
		}).then(function (response) {
			this.setState({
				products: response.data
			});
		}.bind(this));		
	}

	componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}

	render() {
		return (
			<Grid>
				<Row>
					<Col xs={12}>
						<Jumbotron>
							<h1>Hello, world!</h1>
							<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and
									three supporting pieces of content.</p>
							<p><a className="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
						</Jumbotron>
					</Col>
				</Row>
				<Row>
					<Col md={3}>
						{this.state.categories.length ? <Categories key="home-side-categories" categories={this.state.categories} /> : <li>Loading...</li>}
					</Col>
					<Col md={9}>
						<div className="block-grid-xs-1 block-grid-sm-2 block-grid-md-3">
							{this.state.products.length ? this.state.products.map(item =>
								<Product key={item.id} id={item.id} category_id={item.category_id} categories={this.state.categories}
									product_name={item.product_name} url_key={item.url_key}	image={item.image} 
									description={item.description} />
							) : <li>Loading...</li>}
						</div>
					</Col>
				</Row>
			</Grid>
		);
	}
}

module.exports = Home;