"use strict";

import React from "react";
import { Grid, Row, Col, Jumbotron } from "react-bootstrap";

import Categories from "../components/categories";
import Product from "../components/Product";


class Category extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			cat_id: null,
			category: this.props.params.cat,
			categories: [],
			products: []
		};
	}

	componentWillMount() {
		// FU jQuery!!!
		let a1 = $.ajax({		
			url: "http://localhost:3000/api/v1/categories",
			cache: false
		});

		let a2 = a1.then(function (response) {
			this.setState({
				categories: response.data
			});
			let cat = this.state.category[0].toUpperCase() + this.state.category.slice(1);		// make the 1st letter uppercase
			let cat_id = this.state.categories.find(obj => obj.category === cat);
			console.log("cat, cat_id: " , cat, cat_id.id)
			return $.ajax({
				url: "http://localhost:3000/api/v1/categories/" + cat_id.id + "/products",
				cache: false
			});
		}.bind(this));

		a2.done(function (response) {
			this.setState({
				products: response.data
			});
		}.bind(this));
	}

	componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}

	render() {
		return (
			<Grid>
				<Row>
					<Col xs={12}>
						<Jumbotron>
							<h1>Catogories</h1>
							<p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and
									three supporting pieces of content.</p>
							<p><a className="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>
						</Jumbotron>
					</Col>
				</Row>
				<Row>
					<Col md={3}>
						{this.state.categories.length ? <Categories key="home-side-categories" categories={this.state.categories} /> : <li>Loading...</li>}
					</Col>
					<Col md={9}>
						<div className="block-grid-xs-1 block-grid-sm-2 block-grid-md-3">
							{this.state.products.length ? this.state.products.map(item =>
								<Product key={item.id} id={item.id} category_id={item.category_id} categories={this.state.categories}
									product_name={item.product_name} url_key={item.url_key}	image={item.image} 
									description={item.description} />
							) : <li>Loading...</li>}
						</div>
					</Col>
				</Row>
			</Grid>
		);
	}
}

module.exports = Category;