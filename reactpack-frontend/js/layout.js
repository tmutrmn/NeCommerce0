"use strict";

import React from "react";
import { Router, Route, Link, browserHistory } from "react-router";
//import Reflux from "reflux";
import { Row } from "react-bootstrap";

import Navigation from "./components/navigation";
import Footer from "./components/footer";


class Layout extends React.Component {
	constructor(props) {
		super();
		// console.log(props)
		this.state = {
			// categories: []
		};
  }

/*
	componentWillMount() {
		$.ajax({
			url: "http://localhost:3000/api/v1/categories",
			cache: false
		}).then(function (response) {
			console.log(response);
			this.setState({
				categories: response.data
			});
		}.bind(this));
	}

	componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}
*/

	render() {
		return(
			<div>
				<Navigation />
				{React.cloneElement(
          this.props.children,
          this.state
        )}				
				<Footer />
			</div>
		);
	}
}
/*{this.props.children}*/
module.exports = Layout;
